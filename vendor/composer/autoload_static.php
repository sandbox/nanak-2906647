<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite73adcd3cb2456f6610627de8c360cd1
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PayU\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PayU\\' => 
        array (
            0 => __DIR__ . '/../..' . '/includes/PayU',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite73adcd3cb2456f6610627de8c360cd1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite73adcd3cb2456f6610627de8c360cd1::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
