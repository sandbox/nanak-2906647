<?php

/**
 * Alter the order object, to send store specific data to PayU.
 *
 * Be careful if you need to alter the data that are used to calculate the hash,
 * as it might break your request.
 *
 * @var \PayU\Order $order
 *  The PayU order object.
 */
function hook_commerce_payu_russia_order_alter(&$order) {
  // Add a customer profile billing and delivery company.
  $commerce_order = commerce_order_load_by_number($order->getOrderRef()['value']);

  $order_wrapper = entity_metadata_wrapper('commerce_order', $commerce_order);

  foreach (array_keys(commerce_customer_profile_types()) as $profile_type) {
    if (isset($order_wrapper->$profile_type)) {
      $profiles = $order->getProfiles();

      if (isset($profiles[$profile_type])
      && isset($order_wrapper->$profile_type->field_company)) {
        /** @var \PayU\Profile $profile*/
        $profile = $profiles[$profile_type];
        $profile->setCompany($order_wrapper->$profile_type->field_company->value());
      }
    }
  }
}