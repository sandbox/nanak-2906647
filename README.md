# Commerce PayU Russia

This module adds a payment method to integrate Drupal Commerce with the Live Update module of PayU's Russian version.

## Module configuration
* Enable the module
* Visit Administration » Store » Configuration » Payment methods to enable the payment method
* Visit Administration » Store » Configuration » Payment methods » Editing reaction rule "Commerce PayU Russia" » 
Editing action "Enable payment method: Commerce PayU Russia" to configure the settings.

## Payment method configuration
The following settings are available for configuration.

* Merchant ID: The merchant's ID, available in Control Panel (Account Management / Account Settings)
* Merchant secret key: The merchant's secret key, used to authenticate a request, available in Control Panel (Account
Management / Account Settings)
* Test order: Used to initiate transactions in TEST MODE. If the parameter is active, the PayU payment form
will be pre-filled with test payment details (you don't need any credit card test numbers).
* Debug mode: This checkbox enables two things: the PayU debug mode, used to require the PayU support team's
assistance. If the parameter is active, you can request logs of the communication between your server and PayU. This
will also log all the important steps of the payment process in dblog / syslogs, from the form generation to the
transaction creation. Should be enabled only during implementation, or if you are experiencing issues with payments.
* Unique SKU: If a same SKU exists on different line items, with a different price (i.e. when using
commerce_pricing_attributes), PayU will merge them, and override the price, name and taxe of the other line item,
ending up with an incorrect order total. When checked, a hash is happened to the SKU, to ensure its uniqueness.
(See [ORDER_PCODE[]](https://secure.payu.ru/docs/lu/#order-details))
* Automode: Enabling automode allows to send the customer directly to the payment step, without having to fill in any
information. Some fields needs to be pre-filled in the submitted form: BILL_FNAME, BILL_LNAME, BILL_EMAIL, BILL_PHONE
and BILL_COUNTRYCODE (See [AUTOMODE[]](https://secure.payu.ru/docs/lu/#order-details))
* Order Timeout: Sets the interval in which the order can be placed (optional, takes a number of seconds as a value).
* Timeout URL: Sets the URL for the redirect of the customer, in case the ORDER_TIMEOUT expired (optional). Default to
checkout/[order-id]/payment/back/[payment_redirect_key] is ORDER_TIMEOUT is set, but TIMEOUT_URL is not.

## PayU Portal configuration
The receive Instant Payment Notification (IPN), which is the response from PayU containing all payment payload, you need
to configure the endpoint in the PayU Portal. To do so, visit Account Management > Account Settings and click on the IPN
settings tab. In the URL field, enter http(s)://domain_name/commerce-payu-russia/ipn. You'll receive a green message if
PayU can fetch the URL.
From this screen, you can also chose which information you want to receive in your IPN feedback payload.

## Development Tips

### Local development
You can reach the payment platform from your local without any issue, but you won't be able the receive the IPN
response, hence your order will remain in checkout_payment step if it fails, or the transaction won't be created if it
succeed. To this end, you can use [ngrok](https://ngrok.com/). It's a free tool that allows to expose local servers to
internet, by using an HTTP tunnel. It's quite easy to set up:
* Create an account
* Download the binary
* Add your token (available on your account page)
>```./ngrok authtoken [your-token]```
* Add ```*.ngrok.io``` server alias to your site's VirtualHost and restart Apache
* Finally run 
>```./ngrok http {local_hostname}:{local_port}```

You'll see the ngrok terminal with some statistics, and the forwarder request. Copy the forwarding url of your choice to
your browser, and you should land on your local site, and view all HTTP requests that comes by in the terminal. You can 
also open http://localhost:4040 (or htp://127.0.0.1:4040, depending on your configuration) to view it in your browser, 
with extra informations.

Once this is setup, copy your ngrok.io url, and paste it in Account Management > Account Settings > IPN settings. You
should have a url like http(s)://a1b2c3d4.ngrok.io/commerce-payu-russia/ipn. Hit URL setting, and you should get a green
message. That's it, you will receive your IPN notifications on your local machine.

Even better: by adding ?XDEBUG_SESSION_START={IDEKEY}, you'll be able to use xDebug on these requests.

### Debugging IPN requests
If you are in test mode, you can resubmit the IPN notification. To do so, go to the PayU control panel,
Orders & Reports > Delivery confirmation. Click on an order and hit the "Resend notification". The notification is then
resent, you can analyse it, view the expected hash string, see your server's answer... 