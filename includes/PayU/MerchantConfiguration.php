<?php

namespace PayU;

/**
 * Class MerchantConfiguration
 *
 * Make the payment configuration available to the other components of the
 * payment method.
 *
 * @package PayU
 */
class MerchantConfiguration {

  /**
   * Returns itself.
   * @var bool|\PayU\MerchantConfiguration
   */
  private static $instance = FALSE;

  /**
   * The merchant's ID, available in Control Panel.

   * @var string
   */
  private $merchantId;

  /**
   * Secret key of the merchant, for hash calculation.
   *
   * Available in Control Panel, in the Account Management / Account Settings
   * section.
   *
   * @var string
   */
  private $merchantSecret;

  /**
   * Text parameter, used to initiate transactions in TEST MODE.
   *
   * "TRUE" or "FALSE". If the parameter is active, the PayU payment form will
   * be pre-filled with test payment details (you don't need any credit card
   * test numbers).
   *
   * @var string
   */
  private $testOrder;

  /**
   * Used to require the PayU support team's assistance during implementation.
   *
   * Boolean parameter ("0" or "1"). If the parameter is active, you can request
   * logs of the communication between your server and PayU.
   *
   * @var bool
   */
  private $debug;

  /**
   * Indicates if SKUs should be differentiated.
   *
   * If multiple products are sent (in the same or subsequent transactions) with
   * the same product code, PayU will update the product with the corresponding
   * ORDER_PCODE (overwriting all the other product information - name, price,
   * taxes). But if you are using something like commerce_pricing_attributes,
   * you can end up with a sku with different parameters, including the price.
   * This options solves this issue.
   *
   * @var bool
   */
  private $uniqueSku;

  /**
   * Redirect the customer directly to the payment page.
   *
   * The following field have to be send for this parameter to work:
   * - BILL_FNAME
   * - BILL_LNAME
   * - BILL_EMAIL
   * - BILL_PHONE
   * - BILL_COUNTRYCODE.
   *
   * The validation for the BILL_PHONE field is although relaxed (so if you, as
   * a merchant, do not collect / use the phone number of the customer, you can
   * use a dash "-" as a value).
   *
   * @var bool
   */
  private $autoMode;

  /**
   * Sets the interval in which the order can be placed.
   *
   * Takes a number of seconds as a value.
   *
   * @var string
   */
  private $orderTimeout;

  /**
   * URL for the redirect of the customer if ORDER_TIMEOUT is set and expires.
   *
   * @var string
   */
  private $timeoutUrl;

  /**
   * List of currencies available on the payment platform.
   *
   * @var array
   */
  public static $allowedPricesCurrency = [];

  /**
   * List of languages available on the payment platform.
   *
   * @var array
   */
  public static $supportedLanguages = ['RO', 'EN', 'HU', 'RU', 'DE', 'FR', 'IT', 'ES'];

  /**
   * MerchantConfiguration constructor.
   *
   * Load all the settings of the payment method.
   */
  public function __construct() {
    $instance = commerce_payment_method_instance_load('commerce_payu_russia|commerce_payment_commerce_payu_russia');
    $settings = $instance['settings'];

    $settings += _commerce_payu_russia_settings_form_default();

    $this->setMerchantId($settings['merchant_id'])
      ->setMerchantSecret($settings['merchant_secret_key'])
      ->setDebug($settings['debug'])
      ->setTestOrder($settings['test_order'])
      ->setUniqueSku($settings['unique_sku'])
      ->setAutoMode($settings['automode'])
      ->setOrderTimeout($settings['order_timeout'])
      ->setTimeoutUrl($settings['timeout_url']);

    static::$allowedPricesCurrency = $settings['allowed_currencies'];
  }

  /**
   * Method for self instantiation.
   *
   * @return \PayU\MerchantConfiguration
   */
  public static function getInstance() {
    return self::$instance === FALSE ? new MerchantConfiguration() : self::$instance;
  }

  /**
   * @return string
   */
  public function getMerchantId() {
    return $this->merchantId;
  }

  /**
   * @param string $merchantId
   *
   * @return MerchantConfiguration
   */
  private function setMerchantId($merchantId) {
    $this->merchantId = $merchantId;
    return $this;
  }

  /**
   * @return string
   */
  public function getMerchantSecret() {
    return $this->merchantSecret;
  }

  /**
   * @param string $merchantSecret
   *
   * @return MerchantConfiguration
   */
  private function setMerchantSecret($merchantSecret) {
    $this->merchantSecret = $merchantSecret;
    return $this;
  }

  /**
   * @return bool
   */
  public function getTestOrder() {
    return $this->testOrder;
  }

  /**
   * @param bool $testOrder
   *
   * @return MerchantConfiguration
   */
  public function setTestOrder($testOrder) {
    $this->testOrder = $testOrder;
    return $this;
  }
  /**
   * @return bool
   */
  public function getDebug() {
    return $this->debug;
  }

  /**
   * @param bool $debug
   *
   * @return MerchantConfiguration
   */
  public function setDebug($debug) {
    $this->debug = $debug;
    return $this;
  }

  /**
   * @return bool
   */
  public function getUniqueSku() {
    return $this->uniqueSku;
  }

  /**
   * @param bool $uniqueSku
   *
   * @return MerchantConfiguration
   */
  public function setUniqueSku($uniqueSku) {
    $this->uniqueSku = $uniqueSku;
    return $this;
  }

  /**
   * @return bool
   */
  public function getAutoMode() {
    return $this->autoMode;
  }

  /**
   * @param bool $autoMode
   *
   * @return MerchantConfiguration
   */
  public function setAutoMode($autoMode) {
    $this->autoMode = $autoMode;
    return $this;
  }

  /**
   * @return string
   */
  public function getOrderTimeout() {
    return $this->orderTimeout;
  }

  /**
   * @param string $orderTimeout
   *
   * @return MerchantConfiguration
   */
  public function setOrderTimeout($orderTimeout) {
    $this->orderTimeout = $orderTimeout;
    return $this;
  }

  /**
   * @return string
   */
  public function getTimeoutUrl() {
    return $this->timeoutUrl;
  }

  /**
   * @param string $timeoutUrl
   *
   * @return MerchantConfiguration
   */
  public function setTimeoutUrl($timeoutUrl) {
    $this->timeoutUrl = $timeoutUrl;
    return $this;
  }
}