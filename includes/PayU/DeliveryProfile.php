<?php

namespace PayU;

/**
 * Class DeliveryProfile
 *
 * @package PayU
 *
 * @link https://secure.payu.ru/docs/lu/#billing-delivery
 */
class DeliveryProfile extends Profile {

  const PROFILE_BASE = 'DELIVERY';
}