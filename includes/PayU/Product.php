<?php
/**
 * Created by PhpStorm.
 * User: Morgan Milet
 * Date: 21/08/2017
 * Time: 17:18
 *
 * @file
 */

namespace PayU;

/**
 * Class Product
 *
 * @package PayU
 */
class Product {
  const PRICE_TYPE_NET = 'NET';
  const PRICE_TYPE_GROSS = 'GROSS';

  /**
   * @var
   */
  private $name = ['field_name' => 'ORDER_PNAME[]'];

  /**
   * @var
   */
  private $code = ['field_name' => 'ORDER_PCODE[]'];

  /**
   * @var
   */
  private $info = ['field_name' => 'ORDER_PINFO[]'];

  /**
   * @var array
   */
  private $group = ['field_name' => 'ORDER_PGROUP[]' ];

  /**
   * @var
   */
  private $price = ['field_name' => 'ORDER_PRICE[]'];

  /**
   * @var
   */
  private $priceType = ['field_name' => 'ORDER_PRICE_TYPE[]'];

  /**
   * @var
   */
  private $quantity = ['field_name' => 'ORDER_QTY[]'];

  /**
   * @var
   */
  private $vat = ['field_name' => 'ORDER_VAT[]'];


  public static $md5 = [
    'name',
    'code',
    'info',
    'price',
    'priceType',
    'quantity',
    'vat',
  ];

  /**
   * @return array
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   *
   * @return Product
   *
   * @throws \Exception
   */
  public function setName($name) {
    if(strlen($name) <= 2){
      throw new \Exception(t('The product with the name @name is too short to be sent to Payu.', array('@name'=> $name)));
    }
    $this->name['value'] = substr($name, 0, 155);
    return $this;
  }

  /**
   * @return mixed
   */
  public function getGroup() {
    return $this->group;
  }

  /**
   * @param mixed $group
   *
   * @return Product
   */
  public function setGroup($group) {
    $this->group['value'] = $group;
    return $this;
  }

  /**
   * @return array
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * @param string $code
   *
   * @return Product
   */
  public function setCode($code) {
    $this->code['value'] = substr($code, 0, 50);
    return $this;
  }

  /**
   * @return mixed
   */
  public function getInfo() {
    return $this->info;
  }

  /**
   * @param mixed $info
   *
   * @return Product
   */
  public function setInfo($info) {
    $this->info['value'] = $info;
    return $this;
  }

  /**
   * @return array
   */
  public function getPrice() {
    return $this->price;
  }

  /**
   * @param float $price
   *
   * @return Product
   */
  public function setPrice($price) {
    $this->price['value'] = commerce_currency_amount_to_decimal($price['amount'], $price['currency_code']);
    return $this;
  }

  /**
   * @return mixed
   */
  public function getQuantity() {
    return $this->quantity;
  }

  /**
   * @param mixed $quantity
   *
   * @return Product
   */
  public function setQuantity($quantity) {
    $this->quantity['value'] = $quantity;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getVat() {
    return $this->vat;
  }

  /**
   * @param mixed $vat
   *
   * @return Product
   */
  public function setVat($vat) {
    $this->vat['value'] = $vat * 100;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getPriceType() {
    return $this->priceType;
  }

  /**
   * @param mixed $priceType
   *
   * @return Product
   *
   * @throws \Exception
   */
  public function setPriceType($priceType = self::PRICE_TYPE_GROSS) {
    if(!in_array($priceType, array(self::PRICE_TYPE_NET, self::PRICE_TYPE_GROSS))){
      throw new \Exception(t('Price type is not valid.'));
    }

    $this->priceType['value'] = $priceType;
    return $this;
  }
}