<?php

namespace PayU;

/**
 * Class Profile
 *
 * @package PayU
 *
 * @link https://secure.payu.ru/docs/lu/#billing-delivery
 */
abstract class Profile implements ProfileInterface {

  const PROFILE_BASE = '';

  /**
   * Customer's first name.
   *
   * @var array
   */
  protected $firstName = ['field_name' => 'FNAME'];

  /**
   * Customer's last name.
   *
   * @var array
   */
  protected $lastName = ['field_name' => 'LNAME'];

  /**
   * Legal company name for billing / delivery.
   *
   * @var array
   */
  protected $company = ['field_name' => 'COMPANY'];

  /**
   * Phone number.
   *
   * @var array
   */
  protected $phone = ['field_name' => 'PHONE'];

  /**
   * Customer's/Company's address.
   *
   * @var array
   */
  protected $address = ['field_name' => 'ADDRESS'];

  /**
   * Customer's/Company's address (additional).
   *
   * @var array
   */
  protected $address2 = ['field_name' => 'ADDRESS2'];

  /**
   * Customer's/Company's ZIP/Postal Code.
   *
   * @var array
   */
  protected $zipcode = ['field_name' => 'ZIPCODE'];

  /**
   * City.
   *
   * @var array
   */
  protected $city = ['field_name' => 'CITY'];

  /**
   * State/County.
   *
   * @var array
   */
  protected $state = ['field_name' => 'STATE'];

  /**
   * Country Code.
   *
   * @var array
   */
  protected $countryCode = ['field_name' => 'COUNTRYCODE'];

  /**
   * Profile constructor.
   *
   * Add a prefix for field name, depending if we instanciate a Billing or
   * Delivery profile.
   */
  public function __construct() {
    foreach ($this as &$field) {
      $field['field_name'] = sprintf('%s_%s', static::PROFILE_BASE, $field['field_name']);
    }
  }

  /**
   * Set all fields defined in commerce_customer_address field into the profile.
   *
   * @param array $address
   */
  public function setAddressAttributes($address) {
    $this->setFirstName($address['first_name']);
    $this->setLastName($address['last_name']);
    $this->setAddress($address['thoroughfare']);
    $this->setAddress2($address['premise']);
    $this->setZipcode($address['postal_code']);
    $this->setCity($address['locality']);
    $this->setState($address['administrative_area']);
    $this->setCountryCode($address['country']);
  }

  /**
   * @return array
   */
  public function getFirstName() {
    return $this->firstName;
  }

  /**
   * @param array $firstName
   *
   * @return Profile
   */
  public function setFirstName($firstName) {
    $this->firstName['value'] = $firstName;
    return $this;
  }

  /**
   * @return array
   */
  public function getLastName() {
    return $this->lastName;
  }

  /**
   * @param array $lastName
   *
   * @return Profile
   */
  public function setLastName($lastName) {
    $this->lastName['value'] = $lastName;
    return $this;
  }

  /**
   * Legal company name for billing.
   *
   * @return array
   */
  public function getCompany() {
    return $this->company;
  }

  /**
   * @param array $company
   *
   * @return Profile
   */
  public function setCompany($company) {
    $this->company['value'] = $company;
    return $this;
  }

  /**
   * @return array
   */
  public function getPhone() {
    return $this->phone;
  }

  /**
   * @param array $phone
   *
   * @return Profile
   */
  public function setPhone($phone) {
    $this->phone['value'] = $phone;
    return $this;
  }

  /**
   * @return array
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * @param array $address
   *
   * @return Profile
   */
  public function setAddress($address) {
    $this->address['value'] = $address;
    return $this;
  }

  /**
   * @return array
   */
  public function getAddress2() {
    return $this->address2;
  }

  /**
   * @param array $address2
   *
   * @return Profile
   */
  public function setAddress2($address2) {
    $this->address2['value'] = $address2;
    return $this;
  }

  /**
   * @return array
   */
  public function getZipcode() {
    return $this->zipcode;
  }

  /**
   * @param array $zipcode
   *
   * @return Profile
   */
  public function setZipcode($zipcode) {
    $this->zipcode['value'] = $zipcode;
    return $this;
  }

  /**
   * @return array
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * @param array $city
   *
   * @return Profile
   */
  public function setCity($city) {
    $this->city['value'] = $city;
    return $this;
  }

  /**
   * @return array
   */
  public function getState() {
    return $this->state;
  }

  /**
   * @param array $state
   *
   * @return Profile
   */
  public function setState($state) {
    $this->state['value'] = $state;
    return $this;
  }

  /**
   * @return array
   */
  public function getCountryCode() {
    return $this->countryCode;
  }

  /**
   * @param array $countryCode
   *
   * @return Profile
   */
  public function setCountryCode($countryCode) {
    $this->countryCode['value'] = $countryCode;
    return $this;
  }
}