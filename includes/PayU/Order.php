<?php

namespace PayU;

/**
 * Class Order
 *
 * @package PayU
 */
class Order {

  /**
   * The merchant's ID.
   *
   * Available in Control Panel (Account Management / Account Settings)
   *
   * @var array
   */
  private $merchantId = ['field_name' => 'MERCHANT'];

  /**
   * Order reference number in merchant's system.
   *
   * For easier order identification.
   * @var array
   */
  private $orderRef = ['field_name' => 'ORDER_REF'];

  /**
   * The date when the order is initiated in the system.
   *
   * In YYYY-MM-DD HH:MM:SS format (e.g.: "2012-05-01 21:15:45").
   *
   * @var array
   */
  private $orderDate = ['field_name' => 'ORDER_DATE'];

  /**
   * An array of Payu\Product products.
   *
   * @var array
   */
  private $products = ['field_name' => 'PRODUCTS'];

  /**
   * * An array of Payu\Profile profiles.
   *
   * @var array
   */
  private $profiles = ['field_name' => 'PROFILES'];

  /**
   * Shipping costs for the order.
   *
   * @var array
   */
  private $orderShipping = ['field_name' => 'ORDER_SHIPPING'];

  /**
   * The currency for this order.
   *
   * The currency in which the prices, taxes, shipping costs and discounts are
   * expressed. Accepted values: RON, EUR, USD. If the parameter is not
   * specified, the default value is RON. To transact a different currency than
   * the one in which the prices are specified, use the CURRENCY parameter.
   *
   * @var array
   */
  private $pricesCurrency = ['field_name' => 'PRICES_CURRENCY'];

  /**
   * Charging in another currency than the one in which prices are specified.
   *
   * If you have more than one currency activated for your account, the
   * customers can choose in the payment form the currency in which they will
   * make the payment. If you want to force the order's collecting using another
   * currency than the one in which the prices are set (PRICES_CURRENCY), you
   * can send the optional parameter.
   *
   * @var array
   */
  private $currency = ['field_name' => 'CURRENCY'];

  /**
   * The discount value for the order.
   *
   * Positive number, with "." as a decimal separator.
   *
   * @var array
   */
  private $discount = ['field_name' => 'DISCOUNT'];

  /**
   * The city where the order delivery is to be made.
   *
   * If the parameter is specified, the customer will not be able to change its
   * value in the PayU payment pages.
   * @var array
   */
  private $destinationCity = ['field_name' => 'DESTINATION_CITY'];

  /**
   * The state (county) where the order delivery is to be made.
   *
   * If the parameter is specified, the customer will not be able to change its
   * value in the PayU payment pages. Possible values for validation are in the
   * "State/County List", available in Control Panel .
   *
   * @var array
   */
  private $destinationState = ['field_name' => 'DESTINATION_STATE'];

  /**
   * The country where the order delivery is to be made.
   *
   * If the parameter is specified, the customer will not be able to change its
   * value in the PayU payment pages. Possible values for validation are in the
   * "Country List", available in Control Panel .
   *
   * @var array
   */
  private $destinationCountry = ['field_name' => 'DESTINATION_COUNTRY'];

  /**
   * The payment method for the transaction.
   * If the parameter is specified, the customer will not be able to change its
   * value in the PayU payment pages. If the parameter is not specified, a
   * drop-down with the payment methods active on the account will be displayed.
   *
   * Possible values:
   * - CCVISAMC
   * - WEBMONEY
   * - QIWI
   * - MAILRU
   * - YANDEX (Do not apply for new customers)
   * - EUROSET_SVYAZNOI
   * - ALFACLICK
   * Special value for merchants that have this method active:
   * - MASTERPASS
   *
   * @var array
   */
  private $payMethod = ['field_name' => 'PAY_METHOD'];

  /**
   * HMAC MD5 signature for the sent data (HMAC is defined in RFC 2104).
   *
   * @var array
   */
  private $orderHash = ['field_name' => 'ORDER_HASH'];

  /**
   * Text parameter, used to initiate transactions in TEST MODE.
   * "TRUE" or "FALSE". If the parameter is active, the PayU payment form will
   * be pre-filled with test payment details (you don't need any credit card
   * test numbers).
   *
   * @var array
   */
  private $testOrder = ['field_name' => 'TESTORDER'];

  /**
   * Used to require the PayU support team's assistance during implementation.
   *
   * Boolean parameter ("0" or "1"). If the parameter is active, you can request
   * logs of the communication between your server and PayU.
   *
   * @var array
   */
  private $debug = ['field_name' => 'DEBUG'];

  /**
   * Allows setting a specific language for the payment interface.
   *
   * Overrides the language detected by the geolocation.
   *
   * Possible values:
   * - RO - Romanian
   * - EN - English
   * - HU - Hungarian
   * - RU - Russian
   * - DE - German
   * - FR - French
   * - IT - Italian
   * - ES - Spanish
   *
   * @var array
   */
  private $language = ['field_name' => 'LANGUAGE'];

  /**
   * Sets the interval in which the order can be placed.
   *
   * Takes a number of seconds as a value.
   *
   * @var array
   */
  private $orderTimeout = ['field_name' => 'ORDER_TIMEOUT'];

  /**
   * URL for the redirect of the customer if ORDER_TIMEOUT is set and expires.
   *
   * @var array
   */
  private $timeoutUrl = ['field_name' => 'TIMEOUT_URL'];

  /**
   * Redirect the customer directly to the payment page.
   *
   * The following field have to be send for this parameter to work:
   * - BILL_FNAME
   * - BILL_LNAME
   * - BILL_EMAIL
   * - BILL_PHONE
   * - BILL_COUNTRYCODE.
   *
   * The validation for the BILL_PHONE field is although relaxed (so if you, as
   * a merchant, do not collect / use the phone number of the customer, you can
   * use a dash "-" as a value).
   * @var array
   */
  private $autoMode = ['field_name' => 'AUTOMODE'];


  /**
   * URL where the form has to be submitted.
   *
   * @var string
   */
  const ACTION_URL =  'https://secure.payu.ru/order/lu.php';


  /**
   * Used for ordering the md5 components.
   *
   * This order is very important to be able to generate the correct validation
   * hash.
   *
   * @var array
   */
  private $md5 = [
    'merchantId',
    'orderRef',
    'orderDate',
    'name',
    'code',
    'info',
    'price',
    'quantity',
    'vat',
    'orderShipping',
    'pricesCurrency',
    'discount',
    'destinationCity',
    'destinationState',
    'destinationCountry',
    'payMethod',
    // Group has to exists in PayU, otherwise you won't be able to access the
    // payment platform.
    'group',
    'priceType',
    'testOrder',
  ];

  /**
   * Order constructor.
   *
   * Sets general information from the configuration.
   *
   * @param $order_reference
   */
  public function __construct($order_reference) {
    $merchant_config = MerchantConfiguration::getInstance();

    $this->setMerchantId($merchant_config->getMerchantId())
      ->setTestOrder($merchant_config->getTestOrder())
      ->setDebug($merchant_config->getDebug())
      ->setAutoMode($merchant_config->getAutoMode())
      ->setOrderTimeout($merchant_config->getOrderTimeout())
      ->setTimeoutUrl($merchant_config->getTimeoutUrl())
      ->setOrderRef($order_reference)
      ->setOrderDate(date('Y-m-d H:i:s'));
  }

  /**
   * Add a product to the products array.
   *
   * @param \PayU\Product $product
   */
  public function addProduct(Product $product) {
    $this->products['value'][] = $product;
  }

  /**
   * Add a profile to the profiles array.
   *
   * @param \PayU\ProfileInterface $profile
   */
  public function addProfile(ProfileInterface $profile, $profile_type) {
    $this->profiles['value'][$profile_type] = $profile;
  }

  /**
   * Generates the order hash.
   */
  public function generateOrderHash() {
    $hs = new HashServices();
    $this->setOrderHash($hs->generateOrderHash($this));
  }

  /**
   * Generates the order form to be submitted to PayU.
   *
   * @param object $commerce_order
   *  The commerce order being processed.
   *
   * @return array $form
   *  The form array to render.
   */
  public function generateOrderForm($commerce_order) {
    $form = [];

    foreach ($this as $field) {
      if (isset($field['field_name'])) {
        switch($field['field_name']) {
          case 'PRODUCTS':
            $form += $this->productsFormElement($field['value']);
            break;

          case 'PROFILES':
            foreach ($field['value'] as $profile) {
              $form += $this->profilesFormElement($profile);
            }
            break;

          default:
            if (isset($field['value'])) {
              $form += $this->basicFormElement($field);
            }
            break;
        }
      }
    }

    // Set the payment return URL.
    $form['BACK_REF'] = [
      '#type' => 'hidden',
      '#value' => url('checkout/' . $commerce_order->order_id . '/payment/return/' . $commerce_order->data['payment_redirect_key'], array('absolute' => TRUE)),
    ];

    // If the timeout got configured, but the url wasn't, send to the payment
    // back url.
    if (!is_null($this->getOrderTimeout()['value']) && is_null($this->getTimeoutUrl()['value'])) {
      $form['TIMEOUT_URL'] = [
        '#type' => 'hidden',
        '#value' => url('checkout/' . $commerce_order->order_id . '/payment/back/' . $commerce_order->data['payment_redirect_key'],
          [
            'absolute' => TRUE,
            'query' => [
              'timeout' => $this->getOrderTimeout()['value'],
            ],
          ]
        ),
      ];
    }

    $form['#action'] = self::ACTION_URL;

    if (MerchantConfiguration::getInstance()->getDebug()) {
      $message = t('Payment information debug for order @order_number - Form array:<br/>
        <pre>@data</pre>',
        [
          '@order_number' => $this->getOrderRef()['value'],
          '@data' => print_r($this, TRUE),
        ]
      );
      watchdog('commerce_payu_russia', $message, NULL, WATCHDOG_DEBUG);
    }

    return $form;
  }

  /**
   * Add a simple field form element.
   *
   * @param array $field
   *  The field to be added.
   *
   * @return array
   *  The form element of the field.
   */
  private function basicFormElement($field) {
    return [
      $field['field_name'] => [
        '#type' => 'hidden',
        '#value' => isset($field['value']) ? $field['value'] : NULL,
      ]
    ];
  }

  /**
   * Generates the product fields to be attached to the form.
   *
   * @param array $products
   *   An array of products.
   *
   * @return array
   *  The form elements of the products.
   */
  private function productsFormElement($products) {
    $form_elements = [];

    // Get the public properties of the Product class.
    $properties = $this->getClassProperties('PayU\Product');

    foreach ($properties as $property) {
      // Check if there is a getter for this property.
      if (method_exists('Payu\Product', 'get' . ucfirst($property->name))) {
        // Get the propery value of all products, one after the other.
        $form_elements[$property->name] = array_map(function ($obj, $i) use ($property) {
          $field = $obj->{'get' . ucfirst($property->name)}();
          // Add a key to group properties of a product together.
          $field['field_name'] = str_replace('[]', "[$i]", $field['field_name']);
          return $this->basicFormElement($field);
        }, $products, array_keys($products));
      }
    }

    // Unset the properties forms elements that are empty an all products.
    $form = [];
    foreach ($form_elements as $key => $form_element) {
      $empty_field = [];
      foreach ($form_element as $field_name => $field) {
        $key_field_name = key($field);
        if (is_null($field[$key_field_name]['#value']) || empty($field[$key_field_name]['#value'])) {
          $empty_field[$key_field_name] = TRUE;
        }
        $form += $field;
      }
      if (count($form_element) == count($empty_field)) {
        $form = array_diff_key($form, $empty_field);
      }
    }

    return $form;
  }

  /**
   * Generates the fields from the address field to be attached to the form.
   *
   * Extra fields from a customer profile should be attached to the form through
   * hook_commerce_payu_russia_order_alter().
   *
   * @param \PayU\ProfileInterface $profile
   *  The profile to be added.
   *
   * @return array
   *  The form array containing the profile fields.
   */
  private function profilesFormElement($profile) {
    $form = [];
    $properties = $this->getClassProperties('PayU\Profile');

    foreach ($properties as $property) {
      if (method_exists('PayU\Profile', 'get' . ucfirst($property->name))) {
        $field = $profile->{'get' . ucfirst($property->name)}();
        if (isset($field['value']) && !empty($field['value'])) {
          $form += $this->basicFormElement($field);
        }
      }
    }

    return $form;
  }

  /**
   * Get all public properties of a class.
   *
   * @param string $class_name
   *  The class name from which we want to get the properties.
   *
   * @return \ReflectionProperty[]
   *  A array of ReflectionPropety.
   */
  private function getClassProperties($class_name) {
    $reflection_class = new \ReflectionClass($class_name);
    return $reflection_class->getProperties();
  }


  /**
   * @return array
   */
  public function getMerchantId() {
    return $this->merchantId;
  }

  /**
   * @param string $merchantId
   *
   * @return Order
   */
  public function setMerchantId($merchantId) {
    $this->merchantId['value'] = $merchantId;
    return $this;
  }

  /**
   * @return array
   */
  public function getOrderRef() {
    return $this->orderRef;
  }

  /**
   * @param string $orderRef
   *
   * @return Order
   */
  public function setOrderRef($orderRef) {
    $this->orderRef['value'] = $orderRef;
    return $this;
  }

  /**
   * @return array
   */
  public function getOrderDate() {
    return $this->orderDate;
  }

  /**
   * @param string $orderDate
   *
   * @return Order
   */
  public function setOrderDate($orderDate) {
    $this->orderDate['value'] = $orderDate;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getOrderShipping() {
    return $this->orderShipping;
  }

  /**
   * @param int|float $orderShipping
   *
   * @return Order
   */
  public function setOrderShipping($orderShipping) {
    $this->orderShipping['value'] = commerce_currency_amount_to_decimal($orderShipping['amount'], $orderShipping['currency_code']);;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getPricesCurrency() {
    return $this->pricesCurrency;
  }

  /**
   * @param string $pricesCurrency
   *
   * @return Order
   *
   * @throws \Exception
   */
  public function setPricesCurrency($pricesCurrency) {
    if (!in_array($pricesCurrency, MerchantConfiguration::$allowedPricesCurrency)) {
      throw new \Exception(t('The "@currency" currency is not supported by PayU Russia.'), ['@currency' => $pricesCurrency]);
    }

    $this->pricesCurrency['value'] = $pricesCurrency;
    return $this;
  }

  /**
   * @return array
   */
  public function getDiscount() {
    if (!isset($this->discount['value'])) {
      $this->setDiscount(0);
    }

    return $this->discount;
  }

  /**
   * @param int|float $discount
   *
   * @return Order
   */
  public function setDiscount($discount) {
    $this->discount['value'] = $discount;
    return $this;
  }

  /**
   * @return array
   */
  public function getDestinationCity() {
    return $this->destinationCity;
  }

  /**
   * @param string $destinationCity
   *
   * @return Order
   */
  public function setDestinationCity($destinationCity) {
    $this->destinationCity['value'] = $destinationCity;
    return $this;
  }

  /**
   * @return array
   */
  public function getDestinationState() {
    return $this->destinationState;
  }

  /**
   * @param string $destinationState
   *
   * @return Order
   */
  public function setDestinationState($destinationState) {
    $this->destinationState['value'] = $destinationState;
    return $this;
  }

  /**
   * @return array
   */
  public function getDestinationCountry() {
    return $this->destinationCountry;
  }

  /**
   * @param string $destinationCountry
   *
   * @return Order
   */
  public function setDestinationCountry($destinationCountry) {
    $this->destinationCountry['value'] = $destinationCountry;
    return $this;
  }

  /**
   * @return array
   */
  public function getTestOrder() {
    return $this->testOrder;
  }

  /**
   * @param string $test_order
   *
   * @return Order
   */
  public function setTestOrder($test_order) {
    $this->testOrder['value'] = $test_order == TRUE ? 'TRUE' : NULL;
    return $this;
  }

  /**
   * @return array
   */
  public function getDebug() {
    return $this->debug;
  }

  /**
   * @param int $debug
   *
   * @return Order
   */
  public function setDebug($debug) {
    $this->debug['value'] = $debug;
    return $this;
  }

  /**
   * @return array
   */
  public function getLanguage() {
    return $this->language;
  }

  /**
   * @param string $language
   *
   * @return Order
   */
  public function setLanguage($language) {
    if (!in_array($language, MerchantConfiguration::$supportedLanguages)) {
      $language = 'EN';
    }

    $this->language['value'] = $language;
    return $this;
  }

  /**
   * @return array
   */
  public function getOrderTimeout() {
    return $this->orderTimeout;
  }

  /**
   * @param int $orderTimeout
   *
   * @return Order
   */
  public function setOrderTimeout($orderTimeout) {
    $this->orderTimeout['value'] = empty($orderTimeout) ? NULL : $orderTimeout;
    return $this;
  }

  /**
   * @return array
   */
  public function getTimeoutUrl() {
    return $this->timeoutUrl;
  }

  /**
   * @param string $timeoutUrl
   *
   * @return Order
   */
  public function setTimeoutUrl($timeoutUrl) {
    $this->timeoutUrl['value'] = empty($timeoutUrl) ? NULL : $timeoutUrl;
    return $this;
  }

  /**
   * @return array
   */
  public function getPayMethod() {
    return $this->payMethod;
  }

  /**
   * @param string $payMethod
   *
   * @return Order
   */
  public function setPayMethod($payMethod) {
    $this->payMethod['value'] = $payMethod;
    return $this;
  }

  /**
   * Get the order hash. If not set, set it.
   *
   * @return array
   */
  public function getOrderHash() {
    if (is_null($this->orderHash)) {
      $this->generateOrderHash();
    }

    return $this->orderHash;
  }

  /**
   * @param string $orderHash
   *
   * @return Order
   */
  public function setOrderHash($orderHash) {
    $this->orderHash['value'] = $orderHash;
    return $this;
  }

  /**
   * @return array
   */
  public function getMd5() {
    return $this->md5;
  }

  /**
   * @param array $md5
   *
   * @return Order
   */
  public function setMd5($md5) {
    $this->md5 = $md5;
    return $this;
  }

  /**
   * @return array
   */
  public function getAutoMode() {
    return $this->autoMode;
  }

  /**
   * @param int $autoMode
   *
   * @return Order
   */
  public function setAutoMode($autoMode) {
    $this->autoMode['value'] = $autoMode;
    return $this;
  }

  /**
   * @return array
   */
  public function getCurrency() {
    return $this->currency;
  }

  /**
   * @param string $currency
   *
   * @return Order
   */
  public function setCurrency($currency) {
    $this->currency['value'] = $currency;
    return $this;
  }

  /**
   * @return array
   */
  public function getProducts() {
    return $this->products['value'];
  }

  /**
   * @return array
   */
  public function getProfiles() {
    return $this->profiles['value'];
  }
}