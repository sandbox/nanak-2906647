<?php
/**
 * Created by PhpStorm.
 * User: Morgan Milet
 * Date: 23/08/2017
 * Time: 17:06
 *
 * @file
 */

namespace PayU;


interface ProfileInterface {
  public function setAddressAttributes($address);
}