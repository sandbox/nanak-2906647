<?php

namespace PayU;

/**
 * Class HashServices
 *
 * @package PayU
 */
class HashServices {

  /**
   * Secret key used to generate a hash. Defined in PayU > Account settings.
   *
   * @var string $secretKey
   */
  private  $secretKey;

  /**
   * HashServices constructor. Retrieve and set the secret key from the payment
   * method form.
   */
  public function __construct() {
    $merchant_config = MerchantConfiguration::getInstance();
    $this->secretKey = $merchant_config->getMerchantSecret();
  }

  /**
   * Returns an HMAC MD5 for a given string.
   *
   * @param string $string
   *  The string to hash.
   *
   * @return string
   *  The hashed string.
   */
  public function hash_hmac_md5($string) {
    return hash_hmac('md5', $string , $this->secretKey);
  }

  /**
   * Returns the length of a string, concatenated with the string.
   *
   * @param string $string
   *  The string the be concatenated.
   *
   * @return string
   * The concatenated string.
   */
  private function concatLengthString($string) {
    return mb_strlen($string, '8bit') . $string;
  }

  /**
   * Generates a string to be hashed.
   *
   * @param array|string $data
   *  Data to be converted in a string to hash.
   * @return string
   *  The string to be hashed.
   */
  public function getHMACString($data) {
    $hash_string = '';

    if (!is_array($data)) {
      $hash_string .= $this->concatLengthString($data);
    }
    else {
      foreach ($data as $items) {
        $hash_string .= $this->getHMACString($items);
      }
    }

    return $hash_string;
  }

  /**
   * Validates the BACKREF url, used when PayU redirects the customer to the
   * merchant site.
   *
   * @param $order
   *  The Drupal order object.
   *
   * @return bool
   *  Whether or not the request is trustworthy.
   *
   * @link https://secure.payu.ru/docs/lu/#one-step-checkout
   */
  public function validateBackrefUrl($order) {
    // Rebuild the url, without the control key.
    $url = url(sprintf('/checkout/%s/payment/return/%s', $order->order_id, $order->data['payment_redirect_key']), [
      'absolute' => TRUE,
      'query' => array_diff_key($_REQUEST, ['ctrl' => 1]),
    ]);

    $hash_string = $this->concatLengthString($url);

    $hash_service = new HashServices();
    $valid = $hash_service->hash_hmac_md5($hash_string) == $_REQUEST['ctrl'];

    if (MerchantConfiguration::getInstance()->getDebug()) {
      $message = t('Payment information debug for order @order_id - validateBackRefUrl:<br />
        <strong>hash_string:</strong> @hash_string<br />
        <strong>hash:</strong> @hash<br />
        <strong>ctrl:</strong> @ctrl<br />
        <strong>request:</strong> <pre>@request</pre>',
        [
          '@order_id' => $order->order_id,
          '@hash_string' => $hash_string,
          '@hash' => $hash_service->hash_hmac_md5($hash_string),
          '@ctrl' => $_REQUEST['ctrl'],
          '@request' => $_REQUEST,
        ]
      );

      watchdog('commerce_payu_russia', $message, NULL, $valid ? WATCHDOG_DEBUG : WATCHDOG_ERROR);
    }

    return $valid;
  }

  /**
   * Validates the feedback sent by PayU once they processed a payment.
   *
   * @return bool
   *  Whether or not the request is trustworthy.
   */
  public function validateIPNFeedback() {
    $hash_string = $this->getHMACString(array_diff_key($_POST, ['HASH' => 1]));

    $hash_service = new HashServices();
    $valid = $hash_service->hash_hmac_md5($hash_string) == $_POST['HASH'];

    if (MerchantConfiguration::getInstance()->getDebug()) {
      $message = t('Payment information debug for order @order_id - validateIPNFeedback:<br />
        <strong>hash_string:</strong> @hash_string<br />
        <strong>hash:</strong> @hash<br />
        <strong>ctrl:</strong> @ctrl<br />
        <strong>post:</strong>: <pre>@post</pre>',
        [
          '@order_id' => $_POST['REFNOEXT'],
          '@hash_string' => $hash_string,
          '@hash' => $hash_service->hash_hmac_md5($hash_string),
          '@ctrl' => $_POST['HASH'],
          '@post' => print_r($_POST, TRUE),
        ]
      );
      watchdog('commerce_payu_russia', $message, NULL, $valid ? WATCHDOG_DEBUG : WATCHDOG_ERROR);
    }

    return $valid;
  }

  /**
   * Generates the hash of the order to be sent.
   *
   * @param \PayU\Order $payu_order
   *
   * @return string
   */
  public function generateOrderHash($payu_order) {
    $hash_string = '';
    foreach ($payu_order->getMd5() as $attribute) {
      if (method_exists(Order::class, 'get' . ucfirst($attribute)) && ($payu_order->{'get' . ucfirst($attribute)}() !== NULL)) {
        $hash_string .= $this->basicAttributeHashCalculation($payu_order->{'get' . ucfirst($attribute)}());
      }
      elseif (method_exists(Product::class, 'get' . ucfirst($attribute))) {
        $hash_string .= $this->productAttributeHashCalculation($payu_order->getProducts(), $attribute);
      }
    }

    $hash_service = new HashServices();
    if (MerchantConfiguration::getInstance()->getDebug()) {
      $message = t('Payment information debug for order @order_number - generateOrderHash:<br/>
        <pre>@data</pre>
        <strong>hash_string:</strong> @hash_string<br />
        <strong>hash:</strong> @hash',
        [
          '@order_number' => $payu_order->getOrderRef()['value'],
          '@data' => print_r($payu_order, TRUE),
          '@hash_string' => $hash_string,
          '@hash' => $hash_service->hash_hmac_md5($hash_string),
        ]
      );
      watchdog('commerce_payu_russia', $message, NULL, WATCHDOG_DEBUG);
    }

    return $hash_service->hash_hmac_md5($hash_string);
  }

  /**
   * Returns the length of a string, concatenated with the string, if set.
   *
   * @param array $field
   *  The field to process.
   *
   * @return string
   *  The concatenated string. Empty if the value key is not set.
   */
  private function basicAttributeHashCalculation($field) {
    if (isset($field['value'])) {
      return $this->concatLengthString($field['value']);
    }

    return '';
  }

  /**
   * Returns the string to be hashed for a product property.
   *
   * @param array $products
   *  An array of products.
   * @param $attribute
   *  The Payu\Product attribute to retrieve.
   *
   * @return string
   */
  private function productAttributeHashCalculation($products, $attribute) {
    // array_column() does not work with array of object before PHP7, so we're
    // using array_map instead to get the attribute of all objects in the array
    // at once.
    $fields = array_map(function ($product) use ($attribute) {
      $field = $product->{'get' . ucfirst($attribute)}();
      if (isset($field['value'])) {
        return $this->concatLengthString($field['value']);
      }
      return 0;
    }, $products);

    // If all values of an attribute of a product are equal to 0, then it
    // shouldn't be added to the main hash_string.
    if (empty(array_filter($fields))) {
      return '';
    }

    // If at least one of them is filled in, the attribute will be added to the
    // form for all products, so we have to take into account it's length (0) in
    // the string composition.
    $hash_string = implode('', $fields);

    return $hash_string;
  }

}
