<?php

namespace PayU;

/**
 * Class BillingProfile
 *
 * @package PayU
 *
 * @link https://secure.payu.ru/docs/lu/#billing-delivery
 */
class BillingProfile extends Profile {

  /**
   * Used to prefix the fields share with DeliveryProfile.
   *
   * @var string
   */
  const PROFILE_BASE = 'BILL';

  /**
   * Shopper's ID type - mandatory for UPT.
   *
   * Accepted values: PERSONALID (identity card), PASSPORT (passport),
   * DRVLICENSE (driving license)
   *
   * @var array
   */
  protected $ciType = ['field_name' => 'CITYPE'];

  /**
   * ID Card Series (for RO residents).
   *
   * @var array
   */
  protected $ciSerial = ['field_name' => 'CISERIAL'];

  /**
   * ID Card Number (for RO or TR residents).
   *
   * @var array
   */
  protected $ciNumber = ['field_name' => 'CINUMBER'];

  /**
   * ID Card Issuer (for RO residents).
   *
   * @var array
   */
  protected $ciIssuer = ['field_name' => 'CIISSUER'];

  /**
   * Numeric Personal Code (for RO residents).
   *
   * @var array
   */
  protected $cnp = ['field_name' => 'CNP'];

  /**
   * Company's Fiscal Code (CUI/VAT ID).
   *
   * @var array
   */
  protected $fiscalCode = ['field_name' => 'FISCALCODE'];

  /**
   * Company's Registration Number at the Commerce Registry.
   *
   * @var array
   */
  protected $regNumber = ['field_name' => 'REGNUMBER'];

  /**
   * Company's bank.
   *
   * @var array
   */
  protected $bank = ['field_name' => 'BANK'];

  /**
   * Company's bank account.
   *
   * @var array
   */
  protected $bankAccount = ['field_name' => 'BANKACCOUNT'];

  /**
   * Customer's email address.
   *
   * @var array
   */
  protected $email = ['field_name' => 'EMAIL'];

  /**
   * Fax number.
   *
   * @var array
   */
  protected $fax = ['field_name' => 'FAX'];

  /**
   * @return array
   */
  public function getCiType() {
    return $this->ciType;
  }

  /**
   * @param array $ciType
   *
   * @return BillingProfile
   */
  public function setCiType($ciType) {
    $this->ciType['value'] = $ciType;
    return $this;
  }

  /**
   * @return array
   */
  public function getCiSerial() {
    return $this->ciSerial;
  }

  /**
   * @param array $ciSerial
   *
   * @return BillingProfile
   */
  public function setCiSerial($ciSerial) {
    $this->ciSerial['value'] = $ciSerial;
    return $this;
  }

  /**
   * @return array
   */
  public function getCiNumber() {
    return $this->ciNumber;
  }

  /**
   * @param array $ciNumber
   *
   * @return BillingProfile
   */
  public function setCiNumber($ciNumber) {
    $this->ciNumber['value'] = $ciNumber;
    return $this;
  }

  /**
   * @return array
   */
  public function getCiIssuer() {
    return $this->ciIssuer;
  }

  /**
   * @param array $ciIssuer
   *
   * @return BillingProfile
   */
  public function setCiIssuer($ciIssuer) {
    $this->ciIssuer['value'] = $ciIssuer;
    return $this;
  }

  /**
   * @return array
   */
  public function getCnp() {
    return $this->cnp;
  }

  /**
   * @param array $cnp
   *
   * @return BillingProfile
   */
  public function setCnp($cnp) {
    $this->cnp['value'] = $cnp;
    return $this;
  }

  /**
   * @return array
   */
  public function getFiscalCode() {
    return $this->fiscalCode;
  }

  /**
   * @param array $fiscalCode
   *
   * @return BillingProfile
   */
  public function setFiscalCode($fiscalCode) {
    $this->fiscalCode['value'] = $fiscalCode;
    return $this;
  }

  /**
   * @return array
   */
  public function getRegNumber() {
    return $this->regNumber;
  }

  /**
   * @param array $regNumber
   *
   * @return BillingProfile
   */
  public function setRegNumber($regNumber) {
    $this->regNumber['value'] = $regNumber;
    return $this;
  }

  /**
   * @return array
   */
  public function getBank() {
    return $this->bank;
  }

  /**
   * @param array $bank
   *
   * @return BillingProfile
   */
  public function setBank($bank) {
    $this->bank['value'] = $bank;
    return $this;
  }

  /**
   * @return array
   */
  public function getBankAccount() {
    return $this->bankAccount;
  }

  /**
   * @param array $bankAccount
   *
   * @return BillingProfile
   */
  public function setBankAccount($bankAccount) {
    $this->bankAccount['value'] = $bankAccount;
    return $this;
  }

  /**
   * @return array
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @param array $email
   *
   * @return BillingProfile
   */
  public function setEmail($email) {
    $this->email['value'] = $email;
    return $this;
  }

  /**
   * @return array
   */
  public function getFax() {
    return $this->fax;
  }

  /**
   * @param array $fax
   *
   * @return BillingProfile
   */
  public function setFax($fax) {
    $this->fax['value'] = $fax;
    return $this;
  }

}